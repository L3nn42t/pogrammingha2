﻿using System;
using UEGP3.Core;
using UnityEngine;

namespace UEGP3.PlayerSystem
{
	[RequireComponent(typeof(AudioSource))]
	public class FootStepAudioHandler : MonoBehaviour
	{
		//[Tooltip("The audio event that should be played when the animation event is happening")] [SerializeField]
		//private ScriptableAudioEvent _footstepAudioEvent;

		private AudioSource _audioSource;

		private void Awake()
		{
			_audioSource = GetComponent<AudioSource>();
		}

		// Called as an animation event
		private void DoFootStepSound(ScriptableAudioEvent audioEvent)
		{
			audioEvent.Play(_audioSource); //method and audioEvent get called in the Event
			//_footstepAudioEvent.Play(_audioSource);
		}
	}
}