﻿using UEGP3.PlayerSystem;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;


namespace UEGP3.CustomPlayables
{
	[TrackColor(0.233f, 0435f, 0.893f)]
	[TrackClipType(typeof(DeactivateInputClip))]
	//[TrackBindingType(typeof (Player))] //how do i reference this?

	
	public class DeactivateInputTrack : TrackAsset
	{
		//override
		public override Playable CreateTrackMixer(PlayableGraph Graph, GameObject go, int inputCount)
		{
			return ScriptPlayable<DeactivateInputBehavior>.Create(Graph, inputCount);
		}

	}
}

