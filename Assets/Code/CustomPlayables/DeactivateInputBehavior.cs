﻿using System;
using UEGP3.CameraSystem;
using UEGP3.PlayerSystem;
using UnityEngine;
using UnityEngine.Playables;

namespace UEGP3.CustomPlayables
{
    [Serializable]
    public class DeactivateInputBehavior : PlayableBehaviour
    {
       

        public PlayerController playerController;
        public Player player;
        public ThirdPersonFreeLookCamera camera;
    }

}
