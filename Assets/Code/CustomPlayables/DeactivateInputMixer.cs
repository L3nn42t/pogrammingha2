﻿using UnityEngine.Playables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UEGP3.PlayerSystem;
using UEGP3.CameraSystem;

namespace UEGP3.CustomPlayables
{
	public class DeactivateInputMixer : PlayableBehaviour
	{
		public override void ProcessFrame(Playable playable, FrameData info, object playerData)
		{
			// How many clips are on the current track?
			int inputCount = playable.GetInputCount();

			for (int i = 0; i < inputCount; i++)
			{
				// Is the current clip active?
				float inputWeight = playable.GetInputWeight(i);
				// roughly said: This "Scriptable Object" of our Template Behaviour
				ScriptPlayable<DeactivateInputBehavior> inputPlayable = (ScriptPlayable<DeactivateInputBehavior>)playable.GetInput(i);
				// Because we dont need the "Scriptable Object" but the actual object that we defined
				DeactivateInputBehavior input = inputPlayable.GetBehaviour();

				// If the current clip is being played, execute logic
				if (inputWeight > 0)
				{
					//input.player = Player.FindObjectOfType(Player();

					//input.camera = 
					
					input.camera.enabled = false;
					input.playerController.enabled = false;
					input.player.enabled = false;
					//SceneManager.LoadScene(input.SceneToLoad);
				}
			}
		}
	}
}

