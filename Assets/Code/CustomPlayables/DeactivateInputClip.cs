﻿using System;
using UEGP3.CameraSystem;
using UEGP3.PlayerSystem;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace UEGP3.CustomPlayables
{

	[Serializable]
	public class DeactivateInputClip : PlayableAsset, ITimelineClipAsset
	{
		//public ExposedReference<GameObject> _Splayer;
		//public ExposedReference<GameObject> _Scamera;
		//public GameObject mObject;
		//public ExposedReference<ThirdPersonFreeLookCamera> _camera;

		public DeactivateInputBehavior template = new DeactivateInputBehavior();

		public ClipCaps clipCaps => ClipCaps.None;

		public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
		{
			//DeactivateInputBehavior template = new DeactivateInputBehavior();
			//template.tObject = mObject;

			//template.playerController = _Splayer.Resolve(graph.GetResolver()).GetComponent<PlayerController>();
			//template.player = _Splayer.Resolve(graph.GetResolver()).GetComponent<Player>();
			//template.camera = _Scamera.Resolve(graph.GetResolver()).GetComponent<ThirdPersonFreeLookCamera>();

			var playable = ScriptPlayable<DeactivateInputBehavior>.Create(graph, template);

			

			

			//deactivateInputBehavior.playerController = _cplayercontoller;
			

			return playable;
		}
	}
}

