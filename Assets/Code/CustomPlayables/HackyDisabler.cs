﻿using System.Collections;
using System.Collections.Generic;
using UEGP3.CameraSystem;
using UEGP3.PlayerSystem;
using UnityEngine;

public class HackyDisabler : MonoBehaviour
{

    [SerializeField]
    private Player _player;
    [SerializeField]
    private PlayerController _controller;
    [SerializeField]
    private ThirdPersonFreeLookCamera _camera;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnEnable()
    {
       _player.enabled = false;
      _controller.enabled = false;
    _camera.enabled = false;
    }
    private void OnDisable()
    {
        _player.enabled = true;
        _controller.enabled = true;
        _camera.enabled = true;
    }

    private void disable() // for a 
    {
       _player.enabled = false;
      _controller.enabled = false;
    _camera.enabled = false;
    }
}
